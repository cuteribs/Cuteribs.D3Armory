﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using System.Xml.Linq;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.Resources;
using CuteRibs.D3Armory.ViewModels;
using CuteRibs.MVVMLib;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;

namespace CuteRibs.D3Armory
{
	public partial class App : AppBase
	{
		private static readonly string jsonProfilesFilePath = "profiles.json";
		private static readonly string jsonProfileFilePath = "{0}/{1}/profile.json";
		private static readonly string jsonHeroFilePath = "{0}/{1}/{2}.json";
		private static readonly string itemLargeImageFilePath = "Images/ItemL/{0}.png";
		private static readonly string itemSmallImageFilePath = "Images/ItemS/{0}.png";
		private static readonly string skillLargeImageFilePath = "Images/SkillL/{0}.png";
		private static readonly string skillSmallImageFilePath = "Images/SkillS/{0}.png";

		private static readonly string tooltipFilePath = "Tooltips/{0}";

		private static readonly string itemLargeImageUri = "http://us.media.blizzard.com/d3/icons/items/large/{0}.png";
		private static readonly string itemSmallImageUri = "http://us.media.blizzard.com/d3/icons/items/small/{0}.png";
		private static readonly string skillLargeImageUri = "http://us.media.blizzard.com/d3/icons/skills/64/{0}.png";
		private static readonly string skillSmallImageUri = "http://us.media.blizzard.com/d3/icons/skills/42/{0}.png";

		private static List<ProfileBrief> currentProfiles;
		private static Realm[] realms;

		public static bool IsDesignMode { get { return DesignerProperties.IsInDesignTool; } }

		public static List<ProfileBrief> CurrentProfiles
		{
			get
			{
				if (currentProfiles == null)
				{
					LoadProfiles();
				}

				return currentProfiles;
			}
		}

		public static Realm[] Realms
		{
			get
			{
				if (realms == null)
				{
					realms = new Realm[] { Realm.Americas, Realm.Europe, Realm.Asia };

					foreach (var realm in realms)
					{
						realm.Name = App.GetRealmName(realm);
					}
				}

				return realms;
			}
		}

		public App()
			: base()
		{
			this.IsErrorAlertEnabled = true;
		}

		protected override void OnAppLaunching(LaunchingEventArgs e)
		{
			base.OnAppLaunching(e);

			//Location = Settings.GetSettingValue<string>(App.SettingKeyLocation, "上海市");
			//BackgoundPlay = Settings.GetSettingValue<bool>(App.SettingKeyBackgoundPlay, true);
			//SelectedFrequency = (int)(FMRadio.Instance.Frequency * 10);
		}

		protected override void OnAppClosing(ClosingEventArgs e)
		{
			base.OnAppClosing(e);

			SaveProfiles();
		}

		protected override void OnRootFrameNavigationFailed(NavigationFailedEventArgs e)
		{
			base.OnRootFrameNavigationFailed(e);
		}

		protected override void OnUnhandledException(ApplicationUnhandledExceptionEventArgs e)
		{
			base.OnUnhandledException(e);
		}

		private static void SaveProfiles()
		{
			SaveJsonFile(jsonProfilesFilePath, CurrentProfiles);
		}

		private static void LoadProfiles()
		{
			ProfileBrief[] profiles;

			if (App.IsDesignMode)
			{
				var streamInfo = App.GetResourceStream(new Uri("SampleData/profiles.json", UriKind.Relative));

				if (streamInfo == null)
				{
					return;
				}

				profiles = DeserializeJson<ProfileBrief[]>(streamInfo.Stream);
				streamInfo.Stream.Close();
			}
			else
			{
				string fileName = "profiles.json";
				profiles = LoadJsonFile<ProfileBrief[]>(fileName);
			}

			if (profiles != null)
			{
				currentProfiles = profiles.ToList();
			}
			else
			{
				currentProfiles = new List<ProfileBrief>();
			}
		}

		public static void DeleteProfile(ProfileBrief profile)
		{
			CurrentProfiles.Remove(profile);

			var fileName = string.Format(jsonProfileFilePath, profile.Realm.Region, profile.BattleTag.Tag);

			if (FileExists(fileName))
			{
				var directory = Path.GetDirectoryName(fileName);

				using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
				{
					storage.DeleteWholeDirectory(directory);
				}
			}
		}

		public static void SaveProfile(Profile profile)
		{
			var profileBrief = CurrentProfiles.FirstOrDefault(p => p.Realm.Equals(profile.Realm) && p.BattleTag.Equals(profile.BattleTag));

			if (profileBrief == null)
			{
				CurrentProfiles.Add(profile.CreateBrief());
			}

			string fileName = string.Format(jsonProfileFilePath, profile.Realm.Region, profile.BattleTag.Tag);
			SaveJsonFile(fileName, profile);
			SaveProfiles();
		}

		public static Profile LoadProfile(string region, string battleTag)
		{
			string fileName = string.Format(jsonProfileFilePath, region, battleTag);
			var profile = LoadJsonFile<Profile>(fileName);

			if (profile != null)
			{
				profile.Realm = Realm.Parse(region);
			}

			return profile;
		}

		public static void SaveHero(string region, string battleTag, Hero hero)
		{
			string fileName = string.Format(jsonHeroFilePath, region, battleTag, hero.Id);
			SaveJsonFile(fileName, hero);
		}

		public static Hero LoadHero(string region, string battleTag, uint id)
		{
			string fileName = string.Format(jsonHeroFilePath, region, battleTag, id);
			return LoadJsonFile<Hero>(fileName);
		}

		public static T DeserializeJson<T>(Stream stream)
		{
			using (var reader = new StreamReader(stream))
			{
				return JsonConvert.DeserializeObject<T>(reader.ReadToEnd());
			}
		}

		public static void SaveJsonFile(string filePath, object obj)
		{
			if (App.IsDesignMode)
			{
				return;
			}

			using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
			{
				storage.EnsureDirectory(filePath);

				using (var fileStream = storage.CreateFile(filePath))
				{
					using (var writer = new StreamWriter(fileStream))
					{
						writer.Write(JsonConvert.SerializeObject(obj));
					}
				}
			}
		}

		public static T LoadJsonFile<T>(string fileName)
		{
			if (App.IsDesignMode)
			{
				return default(T);
			}

			using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
			{
				if (storage.FileExists(fileName))
				{
					using (var stream = storage.OpenFile(fileName, FileMode.Open))
					{
						return DeserializeJson<T>(stream);
					}
				}
			}

			return default(T);
		}

		public static bool FileExists(string filePath)
		{
			using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
			{
				return storage.FileExists(filePath);
			}
		}

		public static List<RealmStatus> ParseRealmStatus(string html)
		{
			var realms = new List<RealmStatus>();

			try
			{
				var xDoc = XDocument.Parse(html);
				var xRealms = xDoc.Descendants("{http://www.w3.org/1999/xhtml}div").Where(e => e.Attribute("class") != null && e.Attribute("class").Value == "box");

				foreach (var xRealm in xRealms)
				{
					var realm = new RealmStatus { Name = xRealm.Elements().ElementAt(0).Value.Trim(), Servers = new List<ServerStatus>() };

					foreach (var xServer in xRealm.Descendants("{http://www.w3.org/1999/xhtml}div").Where(e => e.Attribute("class") != null && (e.Attribute("class").Value == "server" || e.Attribute("class").Value == "server alt")))
					{
						var server = new ServerStatus { Name = xServer.Elements().ElementAt(1).Value.Trim() };

						if (xServer.Elements().ElementAt(0).Attribute("class").Value.EndsWith("up"))
						{
							server.Status = "up";
						}
						else
						{
							server.Status = "down";
						}

						realm.Servers.Add(server);
					}

					realms.Add(realm);
				}
			}
			catch (Exception ex)
			{
				ShowAlert(ex.Message);
			}

			return realms;
		}

		public static string GetRealmName(Realm realm)
		{
			string name;

			switch (realm.Region)
			{
				case Region.Europe:
					name = AppRes.RealmEurope;
					break;
				case Region.Asia:
					name = AppRes.RealmAsia;
					break;
				case Region.Americas:
				default:
					name = AppRes.RealmAmericas;
					break;
			}

			return name;
		}

		public static string GetDomain(Realm realm = null)
		{
			var langName = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
			string domain;

			if (realm == null)
			{
				switch (langName)
				{
					case "zh":
						domain = "tw.battle.net";
						break;
					case "ko":
						domain = "kr.battle.net";
						break;
					case "en":
						domain = "us.battle.net";
						break;
					default:
						domain = "eu.battle.net";
						break;
				}
			}
			else
			{
				switch (realm.Region)
				{
					case Region.Americas:
						domain = "us.battle.net";
						break;
					case Region.Asia:
						domain = langName.StartsWith("ko") ? "kr.battle.net" : "tw.battle.net";
						break;
					case Region.Europe:
					default:
						domain = "eu.battle.net";
						break;
				}
			}

			return domain;
		}

		public static string GetItemBackgroundUri(string classId, byte gender)
		{
			return string.Format("/Resources/Images/{0}-{1}.jpg", classId, gender == 0 ? "male" : "female");
		}

		public static string GetItemImageUri(string icon, bool isSmall = false)
		{
			string filePath, uri;

			if (isSmall)
			{
				filePath = itemSmallImageFilePath;
				uri = itemSmallImageUri;
			}
			else
			{
				filePath = itemLargeImageFilePath;
				uri = itemLargeImageUri;
			}

			string localUri = string.Format(filePath, icon);

			if (!App.IsDesignMode)
			{
				if (FileExists(localUri))
				{
					return localUri;
				}
			}

			return string.Format(uri, icon);
		}

		public static string GetSkillImageUri(string icon, bool isSmall = false)
		{
			string filePath, uri;

			if (isSmall)
			{
				filePath = skillSmallImageFilePath;
				uri = skillSmallImageUri;
			}
			else
			{
				filePath = skillLargeImageFilePath;
				uri = skillLargeImageUri;
			}

			string localUri = string.Format(filePath, icon);

			if (!App.IsDesignMode)
			{
				if (FileExists(localUri))
				{
					return localUri;
				}
			}

			return string.Format(uri, icon);
		}

		public static string GetSkillName(string slug)
		{
			var name = AppNames.ResourceManager.GetString("SKILL_" + slug.Replace('-', '_'));
			return name;
		}

		public static string GetRuneName(string slug)
		{
			var name = AppNames.ResourceManager.GetString("RUNE_" + slug.Replace('-', '_'));
			return name;
		}

		public static string GetTooltipHtml(string uri)
		{
			//return Tooltip.FindTooltip(uri);

			string html = null;

			var filePath = string.Format(tooltipFilePath, Helper.GetSHA1(uri));

			using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
			{
				if (storage.FileExists(filePath))
				{
					using (var stream = storage.OpenFile(filePath, FileMode.Open))
					{
						using (var reader = new StreamReader(stream))
						{
							html = reader.ReadToEnd();
						}
					}
				}
			}

			return html;
		}

		public static void SaveTooltipHtml(string uri, string html)
		{
			if (App.IsDesignMode)
			{
				return;
			}

			//Tooltip.SaveTooltip(uri, html);

			var filePath = string.Format(tooltipFilePath, Helper.GetSHA1(uri));

			using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
			{
				storage.EnsureDirectory(filePath);

				using (var fileStream = storage.CreateFile(filePath))
				{
					using (var writer = new StreamWriter(fileStream))
					{
						writer.Write(html);
					}
				}
			}
		}

		public static void ClearCache()
		{
			using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
			{
				var folders = new string[] { 
					Region.Americas.ToString(), 
					Region.Europe.ToString(), 
					Region.Asia.ToString() ,
					"Images",
					"Tooltips"
				};

				foreach (var folder in folders)
				{
					storage.DeleteWholeDirectory(folder);
				}
			}
		}

		public static void ShowAlert(string message)
		{
			MessageBox.Show(message, AppRes.AppName, MessageBoxButton.OK);
		}

		public static MessageBoxResult ShowConfirm(string message)
		{
			return MessageBox.Show(message, AppRes.AppName, MessageBoxButton.OKCancel);
		}
	}
}