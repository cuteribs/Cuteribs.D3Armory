﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using CuteRibs.D3Armory.Resources;

namespace CuteRibs.D3Armory.Views
{
	public partial class PageTitleControl : UserControl
	{
		public string MainTitle
		{
			get { return (string)GetValue(MainTitleProperty); }
			set { SetValue(MainTitleProperty, value); }
		}

		public string SubTitle
		{
			get { return (string)GetValue(SubTitleProperty); }
			set { SetValue(SubTitleProperty, value); }
		}

		public static readonly DependencyProperty MainTitleProperty = DependencyProperty.Register("MainTitle", typeof(string), typeof(PageTitleControl), new PropertyMetadata(""));
		public static readonly DependencyProperty SubTitleProperty = DependencyProperty.Register("SubTitle", typeof(string), typeof(PageTitleControl), new PropertyMetadata(""));

		public PageTitleControl()
		{
			this.InitializeComponent();

			this.mainTextBlock.SetBinding(TextBlock.TextProperty, new Binding("MainTitle") { Source = this, });
			this.subTextBlock.SetBinding(TextBlock.TextProperty, new Binding("SubTitle") { Source = this, });

			this.MainTitle = AppRes.AppName;
		}
	}
}