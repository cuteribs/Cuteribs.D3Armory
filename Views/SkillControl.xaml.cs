﻿using System.Windows.Controls;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.ViewModels;

namespace CuteRibs.D3Armory.Views
{
	public partial class SkillControl : UserControl
	{
		public SkillControl()
		{
			this.InitializeComponent();

			if (App.IsDesignMode)
			{
				var skillSet = new SkillSet
				{
					Skill = new ActiveSkill
					{
						Slug = "elemental-arrow",
						Icon = "demonhunter_elementalarrow",
					},
					Rune = new Rune
					{
						Slug = "elemental-arrow-b",
						Type = "b"
					}
				};
				this.DataContext = new SkillViewModel(1, skillSet);
			}
		}
	}
}