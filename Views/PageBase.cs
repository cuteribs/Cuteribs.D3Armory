﻿using System.ComponentModel;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using CuteRibs.D3Armory.Resources;

namespace CuteRibs.D3Armory.Views
{
	public class PageBase : CuteRibs.MVVMLib.ViewBase
	{
		private static AppRes appRes = new AppRes();

		public PageBase()
			: base()
		{
			this.FontFamily = new FontFamily("Arial");
			this.FontSize = 12d;
			this.Foreground = new SolidColorBrush(Helper.ParseColor(0xA99877));
			this.Background = new SolidColorBrush(Helper.ParseColor(0x12110f));
			this.Orientation = PageOrientation.Portrait;

			this.Resources.Add("AppRes", appRes);

			this.Loaded += delegate
			{
				if (!App.IsDesignMode)
				{
					SystemTray.IsVisible = false;
					this.ApplicationBar.Opacity = 0.7d;
				}
			};
		}

		protected override void OnBackKeyPress(CancelEventArgs e)
		{
			base.OnBackKeyPress(e);

			if (this.ViewModel.ShowProgressBar)
			{
				this.ViewModel.ShowProgressBar = false;
			}
		}
	}
}
