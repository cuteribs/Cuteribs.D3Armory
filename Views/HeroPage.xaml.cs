﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Navigation;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.Resources;
using CuteRibs.D3Armory.ViewModels;
using Microsoft.Phone.Controls;
using System.Windows.Media;

namespace CuteRibs.D3Armory.Views
{
	public partial class HeroPage : PageBase
	{
		private D3Client client = new D3Client();
		private HeroPageViewModel myViewModel;

		public HeroPageViewModel MyViewModel
		{
			get
			{
				if (this.myViewModel == null)
				{
					this.myViewModel = (HeroPageViewModel)this.DataContext;
				}

				return this.myViewModel;
			}
		}

		public Realm Realm { get; set; }
		public string BattleTag { get; set; }
		public uint HeroId { get; set; }

		public HeroPage()
			: base()
		{
			InitializeComponent();

			this.Loaded += delegate
			{
				this.AppButtons[0].Text = AppRes.btnRefresh;
			};
		}

		private void RefreshHero()
		{
			Action<Hero> heroAction = delegate(Hero hero)
			{
				this.Dispatcher.BeginInvoke(delegate
				{
					try
					{
						if (!string.IsNullOrEmpty(hero.ErrorCode))
						{
							App.ShowAlert(string.Format("{0}: {1}", hero.ErrorCode, hero.ErrorReason));
							this.MyViewModel.HeroViewModel = null;
						}
						else
						{
							this.MyViewModel.HeroViewModel = new HeroViewModel(hero);
							App.SaveHero(this.Realm.Region.ToString(), this.BattleTag, hero);
						}
					}
					catch (Exception ex)
					{
						App.ShowAlert(ex.Message);
					}

					this.MyViewModel.ShowProgressBar = false;
				});
			};

			this.Dispatcher.BeginInvoke(delegate
			{
				this.MyViewModel.ShowProgressBar = true;
				this.client.GetHero(this.Realm, this.BattleTag, this.HeroId, heroAction);
			});
		}

		private void LoadData(bool refresh = false)
		{
			if (refresh)
			{
				this.RefreshHero();
			}
			else
			{
				this.Dispatcher.BeginInvoke(delegate
				{
					try
					{
						var hero = App.LoadHero(this.Realm.Region.ToString(), this.BattleTag, this.HeroId);

						if (hero == null)
						{
							this.RefreshHero();
						}
						else
						{
							this.MyViewModel.HeroViewModel = new HeroViewModel(hero);
						}
					}
					catch (Exception ex)
					{
						App.ShowAlert(ex.Message);
					}
				});
			}
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			string region, battleTag, heroId;
			this.NavigationContext.QueryString.TryGetValue("Region", out region);
			this.NavigationContext.QueryString.TryGetValue("BattleTag", out battleTag);
			this.NavigationContext.QueryString.TryGetValue("Hero", out heroId);
			this.Realm = Realm.Parse(region);
			this.BattleTag = battleTag;
			this.HeroId = uint.Parse(heroId);
			this.LoadData();
		}

		protected override void OnBackKeyPress(CancelEventArgs e)
		{
			if (this.popup.IsOpen)
			{
				this.popup.IsOpen = false;
			}
		}

		private void Item_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var control = sender as FrameworkElement;
			var viewModel = control.DataContext as ItemViewModel;

			Action<string> callback = delegate(string html)
			{
				if (!string.IsNullOrWhiteSpace(html))
				{
					this.Dispatcher.BeginInvoke(delegate
					{
						this.wbTooltip.NavigateToString(html);
					});
				}
			};

			this.Dispatcher.BeginInvoke(delegate
			{
				this.MyViewModel.ShowProgressBar = true;
				this.MyViewModel.TooltipBorderColor = viewModel.BorderBrush;
				this.client.GetItemTooltipHtml(viewModel.Item.TooltipParams, callback);
			});
		}

		private void Skill_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var control = sender as FrameworkElement;
			var viewModel = control.DataContext as SkillViewModel;

			Action<string> callback = delegate(string html)
			{
				if (!string.IsNullOrWhiteSpace(html))
				{
					this.Dispatcher.BeginInvoke(delegate
					{
						this.wbTooltip.NavigateToString(html);
					});
				}
			};

			this.Dispatcher.BeginInvoke(delegate
			{
				this.MyViewModel.ShowProgressBar = true;
				this.MyViewModel.TooltipBorderColor = new SolidColorBrush(Helper.ParseColor(0x322A20));
				this.client.GetSkillTooltipHtml(viewModel.TooltipParams, viewModel.RuneType, callback);
			});
		}

		private void WebBrowser_ScriptNotify(object sender, NotifyEventArgs e)
		{
			var browser = sender as WebBrowser;
			browser.Height = double.Parse(e.Value) * 1.3446d;
			this.popup.IsOpen = true;
			this.MyViewModel.ShowProgressBar = false;
		}

		private void Popup_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			this.Dispatcher.BeginInvoke(delegate
			{
				this.popup.IsOpen = false;
			});
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			this.LoadData(true);
		}
	}
}