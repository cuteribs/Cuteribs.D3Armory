﻿using System;
using System.Windows.Input;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.Resources;
using CuteRibs.D3Armory.ViewModels;

namespace CuteRibs.D3Armory.Views
{
	public partial class ProfileAddPage : CuteRibs.D3Armory.Views.PageBase
	{
		private ProfileAddPageViewModel myViewModel;

		public ProfileAddPageViewModel MyViewModel
		{
			get
			{
				if (this.myViewModel == null)
				{
					this.myViewModel = (ProfileAddPageViewModel)this.DataContext;
				}

				return this.myViewModel;
			}
		}

		public Profile profile { get; set; }

		public ProfileAddPage()
			: base()
		{
			InitializeComponent();

			this.Loaded += (s, e) =>
			{
				this.AppButtons[0].Text = AppRes.btnFind;
				this.AppButtons[1].Text = AppRes.btnSave;
				this.tbBattleTag.Focus();
			};

			this.lpRealm.SelectionChanged += (s, e) =>
			{
				if (e.AddedItems != null && e.AddedItems.Count > 0)
				{
					this.tbRealm.DataContext = e.AddedItems[0] as Realm;
				}
			};

			this.tbRealm.Tap += (s, e) =>
			{
				this.lpRealm.Open();
			};

			this.tbBattleTag.KeyDown += (s, e) =>
			{
				if (e.Key == Key.Enter)
				{
					this.FindProfile();
				}
			};
		}

		private void FindProfile()
		{
			Action<Profile> action = delegate(Profile profile)
			{
				this.Dispatcher.BeginInvoke(delegate
				{
					try
					{
						if (!string.IsNullOrEmpty(profile.ErrorCode))
						{
							App.ShowAlert(string.Format("{0}: {1}", profile.ErrorCode, profile.ErrorReason));
							this.MyViewModel.ProfileViewModel = null;
							this.AppButtons[1].IsEnabled = false;
							this.tbBattleTag.Focus();
						}
						else
						{
							this.MyViewModel.ProfileViewModel = new ProfileViewModel(profile);
							this.AppButtons[1].IsEnabled = true;
							this.lbHero.Focus();
						}
					}
					catch (Exception ex)
					{
						App.ShowAlert(ex.Message);
					}

					this.MyViewModel.ShowProgressBar = false;
					this.profile = profile;
				});
			};

			this.Dispatcher.BeginInvoke(delegate
			{
				this.MyViewModel.ShowProgressBar = true;
				var battleTag = new BattleTag(this.tbBattleTag.Text.Trim());
				new D3Client().GetProfile(this.lpRealm.SelectedItem as Realm, battleTag.Tag, action);
			});
		}

		private void SaveProfile()
		{
			if (this.profile == null)
			{
				App.ShowAlert(AppRes.MsgNoProfile);
				this.AppButtons[1].IsEnabled = false;
				this.tbBattleTag.Focus();
			}
			else
			{
				this.Dispatcher.BeginInvoke(delegate
				{
					App.SaveProfile(this.profile);
				});

				App.ShowAlert(AppRes.MsgProfileSave);
			}
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			this.FindProfile();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			this.SaveProfile();
		}
	}
}
