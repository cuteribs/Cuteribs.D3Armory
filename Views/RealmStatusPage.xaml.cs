﻿using System;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.ViewModels;
using CuteRibs.D3Armory.Resources;

namespace CuteRibs.D3Armory.Views
{
	public partial class RealmStatusPage : PageBase
	{
		private RealmStatusPageViewModel myViewModel;

		public RealmStatusPageViewModel MyViewModel
		{
			get
			{
				if (this.myViewModel == null)
				{
					this.myViewModel = (RealmStatusPageViewModel)this.DataContext;
				}

				return this.myViewModel;
			}
		}

		public RealmStatusPage()
		{
			InitializeComponent();

			this.Loaded += delegate
			{
				this.AppButtons[0].Text = AppRes.btnRefresh;
				this.RefreshStatus();
			};
		}

		private void RefreshStatus()
		{
			Action<string> callback = x =>
			{
				this.Dispatcher.BeginInvoke(delegate
				{
					this.MyViewModel.Realms = App.ParseRealmStatus(x);
					this.MyViewModel.ShowProgressBar = false;
				});
			};

			this.Dispatcher.BeginInvoke(delegate
			{
				this.MyViewModel.ShowProgressBar = true;
				new D3Client().GetRealmStatus(callback);
			});
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			this.RefreshStatus();
		}
	}
}