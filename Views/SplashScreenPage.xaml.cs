﻿using System;
using System.ComponentModel;
using System.Windows.Navigation;
using System.Windows.Threading;
using CuteRibs.D3Armory.Resources;
using Microsoft.Phone.Controls;

namespace CuteRibs.D3Armory.Views
{
	public partial class SplashScreenPage : PhoneApplicationPage
	{
		private DispatcherTimer timer = new DispatcherTimer();

		public SplashScreenPage()
		{
			InitializeComponent();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			if (this.timer != null)
			{
				this.timer.Interval = TimeSpan.FromSeconds(2d);
				this.timer.Tick += this.Timer_Tick;
				this.timer.Start();

				this.Dispatcher.BeginInvoke(delegate
				{
					this.tbTitle.Text = AppRes.AppName;
				});
			}
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			this.timer.Stop();
			this.timer.Tick -= this.Timer_Tick;
			this.timer = null;
			this.NavigationService.GoBack();
		}

		protected override void OnBackKeyPress(CancelEventArgs e)
		{
			e.Cancel = true;
		}
	}
}