﻿using System;
using System.Linq;
using System.Windows.Navigation;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.ViewModels;
using CuteRibs.MVVMLib;
using System.Windows;
using Microsoft.Phone.Tasks;
using CuteRibs.D3Armory.Resources;
using System.Globalization;
using Microsoft.Phone.Controls;

namespace CuteRibs.D3Armory.Views
{
	public partial class ProfilesPage : PageBase
	{
		private static bool isSplashScreenLoaded;

		private ProfilesPageViewModel myViewModel;

		public ProfilesPageViewModel MyViewModel
		{
			get
			{
				if (this.myViewModel == null)
				{
					this.myViewModel = (ProfilesPageViewModel)this.DataContext;
				}

				return this.myViewModel;
			}
		}

		public ProfilesPage()
			: base()
		{
			InitializeComponent();

			this.Loaded += delegate
			{
				this.AppButtons[0].Text = AppRes.btnAdd;
				this.AppButtons[1].Text = AppRes.btnServer;
				this.AppButtons[2].Text = AppRes.btnHelp;
				this.AppMenus[0].Text = AppRes.mnuRate;
				this.AppMenus[1].Text = AppRes.mnuFeedback;
				this.AppMenus[2].Text = AppRes.mnuShare;

				this.AppButtons[2].IsEnabled = false;
				this.AppMenus[2].IsEnabled = false;
			};
		}

		private void RefreshData()
		{
			this.MyViewModel.ProfileGroups = null;
			this.MyViewModel.ProfileGroups = App.CurrentProfiles.OrderBy(p => p.BattleTag).GroupBy(p => p.Realm).OrderBy(g => g.Key).Select(g => new ProfileGroup(g.Key, g)).ToList();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			if (!isSplashScreenLoaded)
			{
				isSplashScreenLoaded = true;
				this.NavigationService.Navigate("/Views/SplashScreenPage.xaml");
				return;
			}

			base.OnNavigatedTo(e);

			this.Dispatcher.BeginInvoke(delegate
			{
				this.RefreshData();
			});
		}

		private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var profile = (sender as FrameworkElement).DataContext as ProfileBrief;
			this.NavigationService.Navigate(string.Format("/Views/ProfilePage.xaml?Region={0}&BattleTag={1}", profile.Realm.Region, profile.BattleTag.Tag));
		}

		private void mnuDelete_Click(object sender, EventArgs e)
		{
			var control = sender as FrameworkElement;
			var profile = control.DataContext as ProfileBrief;

			this.Dispatcher.BeginInvoke(delegate
			{
				App.DeleteProfile(profile);
				this.RefreshData();
			});
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			this.NavigationService.Navigate("/Views/ProfileAddPage.xaml");
		}

		private void btnStatus_Click(object sender, EventArgs e)
		{
			this.NavigationService.Navigate("/Views/RealmStatusPage.xaml");
		}

		private void btnAbout_Click(object sender, EventArgs e)
		{
			//this.NavigationService.Navigate("/Views/AboutPage.xaml");
			this.NavigationService.Navigate("/Views/HeroPage.xaml?Region={0}&BattleTag={1}&Hero={2}", "Asia", "Cuteribs-1554", 6875500);
		}

		private void btnRate_Click(object sender, EventArgs e)
		{
			new MarketplaceReviewTask().Show();
		}

		private void btnFeedback_Click(object sender, EventArgs e)
		{
			EmailComposeTask task = new EmailComposeTask();
			task.To = "ericfine2012@live.com";
			task.Bcc = "48657539@qq.com";
			task.Subject = string.Format("{0} ({1}) Feedback", AppRes.AppName, CultureInfo.CurrentCulture.Name);
			task.Show();
		}

		private void btnShare_Click(object sender, EventArgs e)
		{
			//ShareLinkTask task = new ShareLinkTask();
			//task.LinkUri = new Uri("http://www.windowsphone.com/s?appid=c6afb7eb-b79d-43d8-b85c-72060139ef8d", UriKind.Absolute);
			//task.Title = "试用此应用程序";
			//task.Message = "\"本地调频\" 是用于 Windows Phone 的众多优秀应用程序之一. 如需了解有关的详细信息, 请访问 WindowsPhone.com!";
			//task.Show();
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			if (App.ShowConfirm(AppRes.MsgClearCache) == MessageBoxResult.OK)
			{
				App.ClearCache();
			}
		}
	}
}