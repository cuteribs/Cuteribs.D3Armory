﻿using System.Windows.Controls;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.ViewModels;

namespace CuteRibs.D3Armory.Views
{
	public partial class ItemControl : UserControl
	{
		public ItemControl()
		{
			InitializeComponent();

			if (App.IsDesignMode)
			{
				var itemBrief = new ItemBrief
				{
					Id = "ChestArmor_204",
					Name = "Clever Bastion",
					Icon = "chestarmor_204_demo",
					DisplayColor = "yellow",
					TooltipParams = "item/CrgBCIOhjdABEgcIBBUtIhlgHQ1S3awdSJ3LyB2QJv6rHbfx7BodkwJj6iILCAAVrv4BABgMICIwCTjlBEAASARQDmDlBGolCgwIABDRxMStgICA4BsSFQj-uZaHChIHCAQVAZaumDAJOABAAWolCgwIABDkxMStgICA4BsSFQiw4YiZCxIHCAQVAZaumDAJOABAAWolCgwIABD1xMStgICA4BsSFQjT-sbSDRIHCAQVAZaumDAJOABAARjaiMmBDFAAWAI",
					RequiredLevel = 60,
					TypeName = "Rare Chest Armor",
					Type = new ItemType(),
					Gems = new Gem[]
					{
						new Gem { Item = new ItemBrief{ Icon = "emerald_08_demonhunter_male" } },
						new Gem { Item = new ItemBrief{ Icon = "topaz_08_demonhunter_male" } },
						new Gem { Item = new ItemBrief{ Icon = "amethyst_08_demonhunter_male" } },
					}
				};
				this.DataContext = new ItemViewModel(itemBrief, ItemSlot.Torso);
			}
		}
	}
}