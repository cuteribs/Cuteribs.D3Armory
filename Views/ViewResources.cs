﻿using CuteRibs.D3Armory.Resources;

namespace CuteRibs.D3Armory.Views
{
	public class ViewResources
	{
		private static AppRes appRes = new AppRes();

		public AppRes AppRes { get { return appRes; } }
	}
}
