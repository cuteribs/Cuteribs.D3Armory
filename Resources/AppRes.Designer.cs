﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CuteRibs.D3Armory.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppRes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppRes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CuteRibs.D3Armory.Resources.AppRes", typeof(AppRes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Act I.
        /// </summary>
        public static string Act1 {
            get {
                return ResourceManager.GetString("Act1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Act II.
        /// </summary>
        public static string Act2 {
            get {
                return ResourceManager.GetString("Act2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Act III.
        /// </summary>
        public static string Act3 {
            get {
                return ResourceManager.GetString("Act3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Act IV.
        /// </summary>
        public static string Act4 {
            get {
                return ResourceManager.GetString("Act4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diablo 3 Armory.
        /// </summary>
        public static string AppName {
            get {
                return ResourceManager.GetString("AppName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Arcane Resist.
        /// </summary>
        public static string ArcaneResist {
            get {
                return ResourceManager.GetString("ArcaneResist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Armor.
        /// </summary>
        public static string Armor {
            get {
                return ResourceManager.GetString("Armor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Level {0} ({1}).
        /// </summary>
        public static string ArtisanLevel {
            get {
                return ResourceManager.GetString("ArtisanLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Artisans.
        /// </summary>
        public static string Artisans {
            get {
                return ResourceManager.GetString("Artisans", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Attributes.
        /// </summary>
        public static string Attributes {
            get {
                return ResourceManager.GetString("Attributes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barbarian.
        /// </summary>
        public static string Barbarian {
            get {
                return ResourceManager.GetString("Barbarian", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blacksmith.
        /// </summary>
        public static string Blacksmith {
            get {
                return ResourceManager.GetString("Blacksmith", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to add.
        /// </summary>
        public static string btnAdd {
            get {
                return ResourceManager.GetString("btnAdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to delete.
        /// </summary>
        public static string btnDelete {
            get {
                return ResourceManager.GetString("btnDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to find.
        /// </summary>
        public static string btnFind {
            get {
                return ResourceManager.GetString("btnFind", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to help.
        /// </summary>
        public static string btnHelp {
            get {
                return ResourceManager.GetString("btnHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to refresh.
        /// </summary>
        public static string btnRefresh {
            get {
                return ResourceManager.GetString("btnRefresh", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to save.
        /// </summary>
        public static string btnSave {
            get {
                return ResourceManager.GetString("btnSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to server.
        /// </summary>
        public static string btnServer {
            get {
                return ResourceManager.GetString("btnServer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cold Resist.
        /// </summary>
        public static string ColdResist {
            get {
                return ResourceManager.GetString("ColdResist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Critical Chance.
        /// </summary>
        public static string CritChance {
            get {
                return ResourceManager.GetString("CritChance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Damage.
        /// </summary>
        public static string Damage {
            get {
                return ResourceManager.GetString("Damage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Damage Increase.
        /// </summary>
        public static string DamageIncrease {
            get {
                return ResourceManager.GetString("DamageIncrease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Damage Reduction.
        /// </summary>
        public static string DamageReduction {
            get {
                return ResourceManager.GetString("DamageReduction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Demon Hunter.
        /// </summary>
        public static string DemonHunter {
            get {
                return ResourceManager.GetString("DemonHunter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dexterity.
        /// </summary>
        public static string Dexterity {
            get {
                return ResourceManager.GetString("Dexterity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Elite Kills.
        /// </summary>
        public static string EliteKills {
            get {
                return ResourceManager.GetString("EliteKills", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fire Resist.
        /// </summary>
        public static string FireResist {
            get {
                return ResourceManager.GetString("FireResist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hardcore.
        /// </summary>
        public static string Hardcore {
            get {
                return ResourceManager.GetString("Hardcore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hardcore Kills.
        /// </summary>
        public static string HardcoreKills {
            get {
                return ResourceManager.GetString("HardcoreKills", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hell.
        /// </summary>
        public static string Hell {
            get {
                return ResourceManager.GetString("Hell", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heroes.
        /// </summary>
        public static string Heroes {
            get {
                return ResourceManager.GetString("Heroes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hero.
        /// </summary>
        public static string HeroPageTitle {
            get {
                return ResourceManager.GetString("HeroPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inferno.
        /// </summary>
        public static string Inferno {
            get {
                return ResourceManager.GetString("Inferno", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Intelligence.
        /// </summary>
        public static string Intelligence {
            get {
                return ResourceManager.GetString("Intelligence", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Items.
        /// </summary>
        public static string Items {
            get {
                return ResourceManager.GetString("Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jeweler.
        /// </summary>
        public static string Jeweler {
            get {
                return ResourceManager.GetString("Jeweler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kills.
        /// </summary>
        public static string Kills {
            get {
                return ResourceManager.GetString("Kills", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Updated.
        /// </summary>
        public static string LastUpdated {
            get {
                return ResourceManager.GetString("LastUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Life.
        /// </summary>
        public static string Life {
            get {
                return ResourceManager.GetString("Life", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Life Time Kills.
        /// </summary>
        public static string LifeTimeKills {
            get {
                return ResourceManager.GetString("LifeTimeKills", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lightning Resist.
        /// </summary>
        public static string LightningResist {
            get {
                return ResourceManager.GetString("LightningResist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading.......
        /// </summary>
        public static string Loading {
            get {
                return ResourceManager.GetString("Loading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string mnuDelete {
            get {
                return ResourceManager.GetString("mnuDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to send feedback to author.
        /// </summary>
        public static string mnuFeedback {
            get {
                return ResourceManager.GetString("mnuFeedback", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to rate this app.
        /// </summary>
        public static string mnuRate {
            get {
                return ResourceManager.GetString("mnuRate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to share with friends.
        /// </summary>
        public static string mnuShare {
            get {
                return ResourceManager.GetString("mnuShare", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Monk.
        /// </summary>
        public static string Monk {
            get {
                return ResourceManager.GetString("Monk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you really want to clear all the cache resources?.
        /// </summary>
        public static string MsgClearCache {
            get {
                return ResourceManager.GetString("MsgClearCache", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Network unavailable..
        /// </summary>
        public static string MsgNoNetwork {
            get {
                return ResourceManager.GetString("MsgNoNetwork", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No profile to save..
        /// </summary>
        public static string MsgNoProfile {
            get {
                return ResourceManager.GetString("MsgNoProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile is saved..
        /// </summary>
        public static string MsgProfileSave {
            get {
                return ResourceManager.GetString("MsgProfileSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nightmare.
        /// </summary>
        public static string Nightmare {
            get {
                return ResourceManager.GetString("Nightmare", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Progression.
        /// </summary>
        public static string NoProgression {
            get {
                return ResourceManager.GetString("NoProgression", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Normal.
        /// </summary>
        public static string Normal {
            get {
                return ResourceManager.GetString("Normal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Career.
        /// </summary>
        public static string Overview {
            get {
                return ResourceManager.GetString("Overview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Physical Resist.
        /// </summary>
        public static string PhysicalResist {
            get {
                return ResourceManager.GetString("PhysicalResist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Poison Resist.
        /// </summary>
        public static string PoisonResist {
            get {
                return ResourceManager.GetString("PoisonResist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Profile.
        /// </summary>
        public static string ProfileAddPageTitle {
            get {
                return ResourceManager.GetString("ProfileAddPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        public static string ProfilePageTitle {
            get {
                return ResourceManager.GetString("ProfilePageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profiles.
        /// </summary>
        public static string ProfilesPageTitle {
            get {
                return ResourceManager.GetString("ProfilesPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Progression.
        /// </summary>
        public static string Progression {
            get {
                return ResourceManager.GetString("Progression", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AMERICAS.
        /// </summary>
        public static string RealmAmericas {
            get {
                return ResourceManager.GetString("RealmAmericas", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ASIA.
        /// </summary>
        public static string RealmAsia {
            get {
                return ResourceManager.GetString("RealmAsia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EUROPE.
        /// </summary>
        public static string RealmEurope {
            get {
                return ResourceManager.GetString("RealmEurope", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server Status.
        /// </summary>
        public static string RealmStatusPageTitle {
            get {
                return ResourceManager.GetString("RealmStatusPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skills.
        /// </summary>
        public static string Skills {
            get {
                return ResourceManager.GetString("Skills", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strength.
        /// </summary>
        public static string Strength {
            get {
                return ResourceManager.GetString("Strength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BattleTag.
        /// </summary>
        public static string tbBattleTag {
            get {
                return ResourceManager.GetString("tbBattleTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Realm.
        /// </summary>
        public static string tbRealm {
            get {
                return ResourceManager.GetString("tbRealm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Played by Class.
        /// </summary>
        public static string TimePlayedByClass {
            get {
                return ResourceManager.GetString("TimePlayedByClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vitality.
        /// </summary>
        public static string Vitality {
            get {
                return ResourceManager.GetString("Vitality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Witch Doctor.
        /// </summary>
        public static string WitchDoctor {
            get {
                return ResourceManager.GetString("WitchDoctor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wizard.
        /// </summary>
        public static string Wizard {
            get {
                return ResourceManager.GetString("Wizard", resourceCulture);
            }
        }
    }
}
