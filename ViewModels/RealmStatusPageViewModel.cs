﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CuteRibs.D3Armory.Resources;
using CuteRibs.MVVMLib;
using System.IO;

namespace CuteRibs.D3Armory.ViewModels
{
	public class RealmStatusPageViewModel : PageViewModelBase
	{
		#region AppRes
		private static AppRes appRes;

		public AppRes AppRes
		{
			get
			{
				if (appRes == null)
				{
					appRes = new AppRes();
				}

				return appRes;
			}
		}
		#endregion

		private List<RealmStatus> realms;

		public List<RealmStatus> Realms
		{
			get { return this.realms; }
			set
			{
				if (this.realms != value)
				{
					this.realms = value;
					this.NotifyPropertyChanged("Realms");
				}
			}
		}

		public RealmStatusPageViewModel()
			: base()
		{
			this.PageTitle = AppRes.RealmStatusPageTitle;

			if (App.IsDesignMode)
			{
				var streamInfo = App.GetResourceStream(new Uri("SampleData/status.html", UriKind.Relative));

				if (streamInfo == null)
				{
					return;
				}

				using (StreamReader reader = new StreamReader(streamInfo.Stream))
				{
					this.Realms = App.ParseRealmStatus(reader.ReadToEnd());
				}

				streamInfo.Stream.Close();
			}
		}
	}

	public class RealmStatus
	{
		public string Name { get; set; }
		public List<ServerStatus> Servers { get; set; }
	}

	public class ServerStatus
	{
		public string Name { get; set; }
		public string Status { get; set; }
	}
}
