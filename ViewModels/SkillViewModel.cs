﻿using System.Windows;
using System.Windows.Media;
using CuteRibs.D3Armory.Models;
using CuteRibs.MVVMLib;

namespace CuteRibs.D3Armory.ViewModels
{
	public class SkillViewModel : ViewModelBase
	{
		private SkillSet skillSet;
		private Visibility visibility;
		private string skillImageUri;
		private string skillName;
		private string runeName;
		private RectangleGeometry buttonClip;
		private TranslateTransform buttonTransform;
		private Visibility buttonVisibility;

		public int SkillIndex { get; private set; }
		public string TooltipParams { get; set; }
		public string RuneType { get; set; }

		public SkillSet SkillSet
		{
			get { return this.skillSet; }
			set
			{
				if (this.skillSet != value)
				{
					this.skillSet = value;
					this.SetViewModel();
				}
			}
		}

		public Visibility Visibility
		{
			get { return this.visibility; }
			set
			{
				if (this.visibility != value)
				{
					this.visibility = value;
					this.NotifyPropertyChanged("Visibility");
				}
			}
		}

		public string SkillImageUri
		{
			get { return this.skillImageUri; }
			set
			{
				if (this.skillImageUri != value)
				{
					this.skillImageUri = value;
					this.NotifyPropertyChanged("SkillImageUri");
				}
			}
		}

		public string SkillName
		{
			get { return this.skillName; }
			set
			{
				if (this.skillName != value)
				{
					this.skillName = value;
					this.NotifyPropertyChanged("SkillName");
				}
			}
		}

		public string RuneName
		{
			get { return this.runeName; }
			set
			{
				if (this.runeName != value)
				{
					this.runeName = value;
					this.NotifyPropertyChanged("RuneName");
				}
			}
		}

		public RectangleGeometry ButtonClip
		{
			get { return this.buttonClip; }
			set
			{
				if (this.buttonClip != value)
				{
					this.buttonClip = value;
					this.NotifyPropertyChanged("ButtonClip");
				}
			}
		}

		public TranslateTransform ButtonTransform
		{
			get { return this.buttonTransform; }
			set
			{
				if (this.buttonTransform != value)
				{
					this.buttonTransform = value;
					this.NotifyPropertyChanged("ButtonTransform");
				}
			}
		}

		public Visibility ButtonVisibility
		{
			get { return this.buttonVisibility; }
			set
			{
				if (this.buttonVisibility != value)
				{
					this.buttonVisibility = value;
					this.NotifyPropertyChanged("ButtonVisibility");
				}
			}
		}

		public SkillViewModel(int skillIndex, SkillSet skillSet = null)
		{
			this.SkillIndex = skillIndex;
			this.SkillSet = skillSet;
		}

		private void SetViewModel()
		{
			if (this.SkillSet == null || this.SkillSet.Skill == null)
			{
				this.Visibility = Visibility.Collapsed;
			}
			else
			{
				this.Visibility = Visibility.Visible;
				this.TooltipParams = this.SkillSet.Skill.TooltipUrl;
				this.SkillImageUri = App.GetSkillImageUri(this.SkillSet.Skill.Icon);
				this.SkillName = App.GetSkillName(this.SkillSet.Skill.Slug);

				if (this.SkillSet.Rune != null)
				{
					this.RuneType = this.skillSet.Rune.Type;
					this.RuneName = App.GetRuneName(this.SkillSet.Rune.Slug);
					this.ButtonVisibility = Visibility.Visible;
					this.SetButtonOffset(this.SkillIndex);
				}
				else
				{
					this.ButtonVisibility = Visibility.Collapsed;
				}
			}
		}

		private void SetButtonOffset(int skillIndex)
		{
			var rect = new Rect(0, 0, 22, 22);

			switch (skillIndex)
			{
				case 1:
					rect.X = 22;
					break;
				case 2:
					rect.Y = 22;
					break;
				case 3:
					rect.X = 22;
					rect.Y = 22;
					break;
				case 4:
					rect.Y = 44;
					break;
				case 5:
					rect.X = 22;
					rect.Y = 44;
					break;
				case 0:
				default:
					break;
			}

			var transform = new TranslateTransform { X = -rect.X, Y = -rect.Y };
			this.ButtonClip = new RectangleGeometry { Rect = rect };
			this.ButtonTransform = transform;
		}
	}
}
