﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.Resources;
using CuteRibs.MVVMLib;

namespace CuteRibs.D3Armory.ViewModels
{
	public class ProfileAddPageViewModel : PageViewModelBase
	{
		#region AppRes
		private static AppRes appRes;

		public AppRes AppRes
		{
			get
			{
				if (appRes == null)
				{
					appRes = new AppRes();
				}

				return appRes;
			}
		}
		#endregion

		private ProfileViewModel profileViewModel;
		private Realm[] realms;

		public ProfileViewModel ProfileViewModel
		{
			get { return this.profileViewModel; }
			set
			{
				if (this.profileViewModel != value)
				{
					this.profileViewModel = value;
					this.NotifyPropertyChanged("ProfileViewModel");
				}
			}
		}

		public Realm[] Realms
		{
			get { return this.realms; }
			set
			{
				if (this.realms != value)
				{
					this.realms = value;
					this.NotifyPropertyChanged("Realms");
				}
			}
		}

		public ProfileAddPageViewModel()
			: base()
		{
			this.Realms = App.Realms;

			if (App.IsDesignMode)
			{
				var streamInfo = App.GetResourceStream(new Uri("SampleData/profile.json", UriKind.Relative));

				if (streamInfo == null)
				{
					return;
				}

				var profile = App.DeserializeJson<Profile>(streamInfo.Stream);
				streamInfo.Stream.Close();
				this.ProfileViewModel = new ProfileViewModel(profile);
			}
		}
	}
}
