﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Media;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.Resources;
using CuteRibs.MVVMLib;

namespace CuteRibs.D3Armory.ViewModels
{
	public class HeroPageViewModel : PageViewModelBase
	{
		#region AppRes
		private static AppRes appRes;

		public AppRes AppRes
		{
			get
			{
				if (appRes == null)
				{
					appRes = new AppRes();
				}

				return appRes;
			}
		}
		#endregion

		private HeroViewModel heroViewModel;
		private SolidColorBrush itemTooltipBorderColor;

		public HeroViewModel HeroViewModel
		{
			get { return this.heroViewModel; }
			set
			{
				if (this.heroViewModel != value)
				{
					this.heroViewModel = value;
					this.NotifyPropertyChanged("HeroViewModel");
				}
			}
		}

		public SolidColorBrush TooltipBorderColor
		{
			get { return this.itemTooltipBorderColor; }
			set
			{
				if (this.itemTooltipBorderColor != value)
				{
					this.itemTooltipBorderColor = value;
					this.NotifyPropertyChanged("TooltipBorderColor");
				}
			}
		}

		public HeroPageViewModel()
			: base()
		{
			this.PageTitle = AppRes.HeroPageTitle;

			if (App.IsDesignMode)
			{
				var streamInfo = App.GetResourceStream(new Uri("SampleData/hero.json", UriKind.Relative));
				var hero = App.DeserializeJson<Hero>(streamInfo.Stream);
				streamInfo.Stream.Close();

				this.HeroViewModel = new HeroViewModel(hero);
			}
		}
	}
}
