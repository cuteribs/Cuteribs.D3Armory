﻿using System.Windows.Media;
using CuteRibs.D3Armory.Models;
using CuteRibs.MVVMLib;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media.Imaging;
using System;

namespace CuteRibs.D3Armory.ViewModels
{
	public class ItemViewModel : ViewModelBase
	{
		public ItemBrief Item { get; set; }
		public ItemSlot ItemSlot { get; set; }

		private string backgroundUri;
		private SolidColorBrush borderBrush;
		private string itemUri;
		private Thickness slotOffset,iconOffset;
		private double slotWidth, slotHeight;
		private double iconWidth, iconHeight;

		public string BackgroundUri
		{
			get { return this.backgroundUri; }
			set
			{
				if (this.backgroundUri != value)
				{
					this.backgroundUri = value;
					this.NotifyPropertyChanged("BackgroundUri");
				}
			}
		}

		public SolidColorBrush BorderBrush
		{
			get { return this.borderBrush; }
			set
			{
				if (this.borderBrush != value)
				{
					this.borderBrush = value;
					this.NotifyPropertyChanged("BorderBrush");
				}
			}
		}

		public string ItemUri
		{
			get { return this.itemUri; }
			set
			{
				if (this.itemUri != value)
				{
					this.itemUri = value;
					this.NotifyPropertyChanged("ItemUri");
				}
			}
		}

		public Thickness SlotOffset
		{
			get { return this.slotOffset; }
			set
			{
				if (this.slotOffset != value)
				{
					this.slotOffset = value;
					this.NotifyPropertyChanged("SlotOffset");
				}
			}
		}

		public Thickness IconOffset
		{
			get { return this.iconOffset; }
			set
			{
				if (this.iconOffset != value)
				{
					this.iconOffset = value;
					this.NotifyPropertyChanged("IconOffset");
				}
			}
		}

		public double SlotWidth
		{
			get { return this.slotWidth; }
			set
			{
				if (this.slotWidth != value)
				{
					this.slotWidth = value;
					this.NotifyPropertyChanged("SlotWidth");
				}
			}
		}

		public double SlotHeight
		{
			get { return this.slotHeight; }
			set
			{
				if (this.slotHeight != value)
				{
					this.slotHeight = value;
					this.NotifyPropertyChanged("SlotHeight");
				}
			}
		}

		public double IconWidth
		{
			get { return this.iconWidth; }
			set
			{
				if (this.iconWidth != value)
				{
					this.iconWidth = value;
					this.NotifyPropertyChanged("IconWidth");
				}
			}
		}

		public double IconHeight
		{
			get { return this.iconHeight; }
			set
			{
				if (this.iconHeight != value)
				{
					this.iconHeight = value;
					this.NotifyPropertyChanged("IconHeight");
				}
			}
		}

		public ObservableCollection<GemViewModel> Gems { get; set; }

		public ItemViewModel() : this(null, default(ItemSlot)) { }

		public ItemViewModel(ItemBrief item, ItemSlot itemSlot)
		{
			this.Item = item;
			this.ItemSlot = itemSlot;
			this.Gems = new ObservableCollection<GemViewModel>();

			if (this.Item != null)
			{
				this.BackgroundUri = string.Format("/Resources/Images/{0}.png", this.Item.DisplayColor);
				this.BorderBrush = GetBorderBrush(this.Item.DisplayColor);
				this.ItemUri = App.GetItemImageUri(this.Item.Icon);
				this.SetOffset(this.ItemSlot);

				if (this.Item.Gems != null)
				{
					foreach (var gem in this.Item.Gems)
					{
						this.Gems.Add(new GemViewModel(gem));
					}
				}
			}
		}

		private static SolidColorBrush GetBorderBrush(string displayColor)
		{
			Color color;

			switch (displayColor)
			{
				case "yellow":
					color = Helper.ParseColor(0xB1A73C);
					break;
				case "blue":
					color = Helper.ParseColor(0x6BA9BA);
					break;
				case "orange":
					color = Helper.ParseColor(0xB07B38);
					break;
				case "brown":
				default:
					color = Helper.ParseColor(0x7A5F45);
					break;
			}

			return new SolidColorBrush(color);
		}

		private void SetOffset(ItemSlot itemSlot)
		{
			Thickness slotffset =new Thickness(), iconOffset = new Thickness();
			double slotWidth = 0, slotHeight = 0, iconWidth = 0, iconHeight = 0;

			switch (itemSlot)
			{
				case ItemSlot.Head:
					slotffset = new Thickness(195, 19, 0, 0);
					iconOffset = new Thickness(-2, -31, 0, 0);
					slotWidth = 66;
					slotHeight = 66;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.Torso:
					slotffset = new Thickness(187, 89, 0, 0);
					iconOffset = new Thickness(-2, -25, 0, 0);
					slotWidth = 82;
					slotHeight = 115;
					iconWidth = 82;
					iconHeight = 164;
					break;
				case ItemSlot.Feet:
					slotffset = new Thickness(195, 338, 0, 0);
					iconOffset = new Thickness(-2, -20, 0, 0);
					slotWidth = 66;
					slotHeight = 88;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.Hands:
					slotffset = new Thickness(87, 142, 0, 0);
					iconOffset = new Thickness(-2, -20, 0, 0);
					slotWidth = 66;
					slotHeight = 88;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.Shoulders:
					slotffset = new Thickness(113, 43, 0, 0);
					iconOffset = new Thickness(-2, -20, 0, 0);
					slotWidth = 66;
					slotHeight = 88;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.Legs:
					slotffset = new Thickness(195, 246, 0, 0);
					iconOffset = new Thickness(-2, -20, 0, 0);
					slotWidth = 66;
					slotHeight = 88;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.Bracers:
					slotffset = new Thickness(303, 142, 0, 0);
					iconOffset = new Thickness(-2, -20, 0, 0);
					slotWidth = 66;
					slotHeight = 88;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.MainHand:
					slotffset = new Thickness(87, 297, 0, 0);
					iconOffset = new Thickness(-2, 0, 0, 0);
					slotWidth = 66;
					slotHeight = 130;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.OffHand:
					slotffset = new Thickness(303, 297, 0, 0);
					iconOffset = new Thickness(-2, 0, 0, 0);
					slotWidth = 66;
					slotHeight = 130;
					iconWidth = 64;
					iconHeight = 128;
					break;
				case ItemSlot.Waist:
					slotffset = new Thickness(187, 208, 0, 0);
					iconOffset = new Thickness(-2, -15, 0, 0);
					slotWidth = 82;
					slotHeight = 34;
					iconWidth = 64;
					iconHeight = 64;
					break;
				case ItemSlot.RightFinger:
					slotffset = new Thickness(99, 242, 0, 0);
					iconOffset = new Thickness(-2, -12, 0, 0);
					slotWidth = 42;
					slotHeight = 42;
					iconWidth = 64;
					iconHeight = 64;
					break;
				case ItemSlot.LeftFinger:
					slotffset = new Thickness(315, 242, 0, 0);
					iconOffset = new Thickness(-2, -12, 0, 0);
					slotWidth = 42;
					slotHeight = 42;
					iconWidth = 64;
					iconHeight = 64;
					break;
				case ItemSlot.Neck:
					slotffset = new Thickness(280, 63, 0, 0);
					iconOffset = new Thickness(-2, -1, 0, 0);
					slotWidth = 56;
					slotHeight = 56;
					iconWidth = 52;
					iconHeight = 52;
					break;
				case ItemSlot.None:
				default:
					break;
			}

			this.SlotOffset = slotffset;
			this.SlotWidth = slotWidth;
			this.SlotHeight = slotHeight;
			this.IconOffset = iconOffset;
			this.IconWidth = iconWidth;
			this.IconHeight = iconHeight;
		}
	}

	public class GemViewModel : ViewModelBase
	{
		private string itemUri;

		public string ItemUri
		{
			get { return this.itemUri; }
			set
			{
				if (this.itemUri != value)
				{
					this.itemUri = value;
					this.NotifyPropertyChanged("ItemUri");
				}
			}
		}

		public Gem Gem { get; set; }

		public GemViewModel(Gem gem = null)
		{
			this.Gem = gem;

			if (this.Gem != null)
			{
				this.ItemUri = App.GetItemImageUri(this.Gem.Item.Icon, true);
			}
		}
	}
}
