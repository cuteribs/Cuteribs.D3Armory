﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CuteRibs.D3Armory.Models;
using CuteRibs.MVVMLib;
using CuteRibs.D3Armory.Resources;
using System.Linq;
using System.Windows.Navigation;

namespace CuteRibs.D3Armory.ViewModels
{
	public class ProfilesPageViewModel : PageViewModelBase
	{
		#region AppRes
		private static AppRes appRes;

		public AppRes AppRes
		{
			get
			{
				if (appRes == null)
				{
					appRes = new AppRes();
				}

				return appRes;
			}
		}
		#endregion

		private List<ProfileGroup> profileGroups;

		public List<ProfileGroup> ProfileGroups
		{
			get { return this.profileGroups; }
			set
			{
				if (this.profileGroups != value)
				{
					this.profileGroups = value;
					this.NotifyPropertyChanged("ProfileGroups");
				}
			}
		}

		public ProfilesPageViewModel()
			: base()
		{
			this.PageTitle = AppRes.ProfilesPageTitle;

			if (App.IsDesignMode)
			{
				this.ProfileGroups = App.CurrentProfiles.OrderBy(p => p.BattleTag).GroupBy(p => p.Realm).OrderBy(g => g.Key).Select(g => new ProfileGroup(g.Key, g)).ToList();
			}
		}
	}
}
