﻿using System;
using CuteRibs.D3Armory.Models;
using CuteRibs.D3Armory.Resources;
using CuteRibs.MVVMLib;

namespace CuteRibs.D3Armory.ViewModels
{
	public class ProfilePageViewModel : PageViewModelBase
	{
		#region AppRes
		private static AppRes appRes;

		public AppRes AppRes
		{
			get
			{
				if (appRes == null)
				{
					appRes = new AppRes();
				}

				return appRes;
			}
		}
		#endregion

		private ProfileViewModel profileViewModel;

		public ProfileViewModel ProfileViewModel
		{
			get { return this.profileViewModel; }
			set
			{
				if (this.profileViewModel != value)
				{
					this.profileViewModel = value;
					this.NotifyPropertyChanged("ProfileViewModel");
				}
			}
		}

		public ProfilePageViewModel()
			: base()
		{
			this.PageTitle = AppRes.ProfilePageTitle;

			if (App.IsDesignMode)
			{
				var streamInfo = App.GetResourceStream(new Uri("SampleData/profile.json", UriKind.Relative));
				var profile = App.DeserializeJson<Profile>(streamInfo.Stream);
				streamInfo.Stream.Close();

				this.ProfileViewModel = new ProfileViewModel(profile);
			}
		}
	}
}
