﻿using System.Windows.Media;
using CuteRibs.MVVMLib;
using System.Windows;

namespace CuteRibs.D3Armory.ViewModels
{
	public class SpriteImageViewModel : ViewModelBase
	{
		private RectangleGeometry clip;
		private TranslateTransform transform;

		public RectangleGeometry Clip
		{
			get { return this.clip; }
			set
			{
				if (this.clip != value)
				{
					this.clip = value;
					this.NotifyPropertyChanged("Clip");
				}
			}
		}

		public TranslateTransform Transform
		{
			get { return this.transform; }
			set
			{
				if (this.transform != value)
				{
					this.transform = value;
					this.NotifyPropertyChanged("Transform");
				}
			}
		}

		public SpriteImageViewModel(double x, double y, double width, double height)
		{
			this.Clip = new RectangleGeometry { Rect = new Rect(x, y, width, height) };
			this.Transform = new TranslateTransform { X = -x, Y = -y };
		}
	}
}
