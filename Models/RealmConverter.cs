﻿using System;
using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	public class RealmConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return true;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return Realm.Parse(reader.Value as string);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var realm = value as Realm;

			if (realm == null)
			{
				throw new Exception("Expected Realm object.");
			}

			writer.WriteValue(realm.Region.ToString());
		}
	}
}
