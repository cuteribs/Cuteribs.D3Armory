﻿using System;
using CuteRibs.D3Armory.Resources;

namespace CuteRibs.D3Armory.Models
{
	public class Class : IEquatable<Class>
	{
		public static readonly Class Barbarian = new Class("barbarian");
		public static readonly Class DemonHunter = new Class("demon-hunter");
		public static readonly Class Monk = new Class("monk");
		public static readonly Class WitchDoctor = new Class("witch-doctor");
		public static readonly Class Wizard = new Class("wizard");

		private string id;

		public string Id
		{
			get { return this.id; }
			set
			{
				this.id = value;
				this.SetName();
			}
		}

		public string Name { get; private set; }

		public Class() { }

		public Class(string id)
		{
			this.Id = id;
		}

		private void SetName()
		{
			switch (this.Id)
			{
				case "demon-hunter":
					this.Name = AppRes.DemonHunter;
					break;
				case "monk":
					this.Name = AppRes.Monk;
					break;
				case "witch-doctor":
					this.Name = AppRes.WitchDoctor;
					break;
				case "wizard":
					this.Name = AppRes.Wizard;
					break;
				case "barbarian":
				default:
					this.Name = AppRes.Barbarian;
					break;
			}
		}

		public override int GetHashCode()
		{
			return this.Id.GetHashCode();
		}

		public override string ToString()
		{
			return this.Id;
		}

		public bool Equals(Class other)
		{
			return this.Id.Equals(other.Id);
		}
	}
}
