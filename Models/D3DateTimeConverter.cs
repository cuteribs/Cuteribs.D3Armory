﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CuteRibs.D3Armory.Models
{
	public class D3DateTimeConverter : DateTimeConverterBase
	{
		private static readonly DateTime StartDateTime = new DateTime(1970, 1, 1);

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			long seconds = Convert.ToInt64(reader.Value);
			DateTime time = StartDateTime.AddSeconds(seconds).ToLocalTime();
			return time;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (!(value is DateTime))
			{
				throw new Exception("Expected date object value.");
			}

			long number = Convert.ToInt32(((DateTime)value - StartDateTime).TotalSeconds);
			writer.WriteValue(number);
		}
	}
}
