﻿using System;
using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	public class BattleTagConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(BattleTag);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return new BattleTag(reader.Value as string);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var battleTag = value as BattleTag;

			if (battleTag == null)
			{
				throw new Exception("Expected BattleTag object.");
			}

			writer.WriteValue(battleTag.Tag);
		}
	}
}
