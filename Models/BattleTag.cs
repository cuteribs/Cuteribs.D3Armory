﻿using Newtonsoft.Json;
using System;

namespace CuteRibs.D3Armory.Models
{
	public class BattleTag : IEquatable<BattleTag>, IComparable<BattleTag>
	{
		private string tag;

		public string Tag
		{
			get { return this.tag; }
			set
			{
				this.tag = value;
				this.SetNameNumber();
			}
		}

		public string Name { get; private set; }

		public ushort Number { get; private set; }

		public BattleTag() { }

		public BattleTag(string tag)
		{
			this.Tag = tag.Replace('#', '-');
		}

		private void SetNameNumber()
		{
			if (!string.IsNullOrWhiteSpace(this.tag))
			{
				int index = this.tag.IndexOf('-');

				if (index > 0)
				{
					this.Name = this.tag.Substring(0, index);
					this.Number = ushort.Parse(this.tag.Substring(index + 1));
				}
			}
		}

		public override int GetHashCode()
		{
			return this.Tag.GetHashCode();
		}

		public override string ToString()
		{
			return this.Tag;
		}

		public bool Equals(BattleTag other)
		{
			return this.Tag.Equals(other.Tag);
		}

		public int CompareTo(BattleTag other)
		{
			return this.Tag.CompareTo(other.Tag);
		}
	}
}
