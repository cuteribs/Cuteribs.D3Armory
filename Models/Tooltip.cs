﻿using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Linq;

namespace CuteRibs.D3Armory.Models
{
	[Table(Name = "Tooltip")]
	public class Tooltip
	{
		[Column(IsPrimaryKey = true)]
		public string Uri { get; set; }

		[Column]
		public string Html { get; set; }

		public static void SaveTooltip(string uri, string html)
		{
			using (var dataContext = GetDataContext())
			{
				var tooltip = dataContext.Tooltips.FirstOrDefault(t => t.Uri == uri);

				if (tooltip == null)
				{
					tooltip = new Tooltip { Uri = uri, Html = html };
					dataContext.Tooltips.InsertOnSubmit(tooltip);
				}
				else
				{
					tooltip.Html = html;
				}

				dataContext.SubmitChanges();
			}
		}

		public static string FindTooltip(string uri)
		{
			using (var dataContext = GetDataContext())
			{
				var tooltip = dataContext.Tooltips.FirstOrDefault(t => t.Uri == uri);

				if (tooltip != null)
				{
					return tooltip.Html;
				}

				return null;
			}
		}

		public static void ClearTooltips()
		{
			using (var dataContext = GetDataContext())
			{
				dataContext.DeleteDatabase();
			}
		}

		private static TooltipDataContext GetDataContext()
		{
			var dataContext = new TooltipDataContext();

			if (!dataContext.DatabaseExists())
			{
				dataContext.CreateDatabase();
			}

			return dataContext;
		}
	}

	public class TooltipDataContext : DataContext
	{
		public Table<Tooltip> Tooltips;

		public TooltipDataContext() : base("isostore:/Tooltip.sdf") { }
	}
}
