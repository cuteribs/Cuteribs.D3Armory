﻿using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	#region Item
	/// <summary>
	///	{
	///		"id": "Bow_206",
	///		"name": "Archer's Blast",
	///		"icon": "bow_206",
	///		"displayColor": "yellow",
	///		"tooltipParams": "item/CPLusvsPEgcIBBW_NlaDHeJDQYUdJQ3O1B3cvJUjHeblqfAdrX_nwyILCAEVbUIDABgKIAAwCTjqA0AAUA5g6gM",
	///		"requiredLevel": 60,
	///		"itemLevel": 63,
	///		"bonusAffixes": 0,
	///		"typeName": "Rare Bow",
	///		"type": {},
	///		"dps": {},
	///		"armor": {},
	///		"attacksPerSecond": {},
	///		"minDamage": {},
	///		"maxDamage": {},
	///		"attributes": [],
	///		"attributesRaw": {},
	///		"socketEffects": [],
	///		"salvage": [],
	///		"gems": []
	///	}
	/// </summary>
	[JsonObject]
	public class Item : D3ObjectBase
	{
		public ItemSlot ItemSlot { get; set; }

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("icon")]
		public string Icon { get; set; }

		[JsonProperty("displayColor")]
		public string DisplayColor { get; set; }

		[JsonProperty("tooltipParams")]
		public string TooltipParams { get; set; }

		[JsonProperty("requiredLevel")]
		public byte RequiredLevel { get; set; }

		[JsonProperty("itemLevel")]
		public byte ItemLevel { get; set; }

		[JsonProperty("bonusAffixes")]
		public byte BonusAffixes { get; set; }

		[JsonProperty("typeName")]
		public string TypeName { get; set; }

		[JsonProperty("type")]
		public ItemType Type { get; set; }

		[JsonProperty("dps")]
		public FloatMinMax Dps { get; set; }

		[JsonProperty("attacksPerSecond")]
		public FloatMinMax AttacksPerSecond { get; set; }

		[JsonProperty("minDamage")]
		public IntegerMinMax MinDamage { get; set; }

		[JsonProperty("maxDamage")]
		public IntegerMinMax MaxDamage { get; set; }

		[JsonProperty("attributes")]
		public string[] Attributes { get; set; }

		[JsonProperty("attributesRaw"), JsonIgnore]
		public ItemAttributesRaw AttributesRaw { get; set; }

		[JsonProperty("socketEffects"), JsonIgnore]
		public ItemType SocketEffects { get; set; }

		[JsonProperty("salvage")]
		public ItemSalvage Salvage { get; set; }

		[JsonProperty("gems")]
		public Gem[] Gems { get; set; }

		#region ItemAttributesRaw
		/// <summary>
		/// {
		///		"Attacks_Per_Second_Item": {},
		///		"Damage_Weapon_Bonus_Delta#Physical": {},
		///		"Damage_Weapon_Min#Physical": {},
		///		"Power_Damage_Percent_Bonus#Witchdoctor_PoisonDart": {},
		///		"Damage_Weapon_Delta#Lightning": {},
		///		"Damage_Weapon_Min#Lightning": {},
		///		"Dexterity_Item": {},
		///		"Bow": {},
		///		"Durability_Max": {},
		///		"Vitality_Item": {},
		///		"Damage_Weapon_Delta#Physical": {},
		///		"Durability_Cur": {}
		///	}
		/// </summary>
		[JsonObject]
		public class ItemAttributesRaw
		{
		}
		#endregion

		#region ItemSalvage
		/// <summary>
		/// {
		///		"chance": 1,
		///		"item": {},
		///		"quantity": 1
		///	}
		/// </summary>
		[JsonObject]
		public class ItemSalvage
		{
			[JsonProperty("chance")]
			public double Chance { get; set; }

			[JsonProperty("item")]
			public Item Item { get; set; }

			[JsonProperty("quantity")]
			public byte quantity { get; set; }
		}
		#endregion

		public ItemBrief CreateBrief()
		{
			return new ItemBrief
			{
				Id = this.Id,
				Name = this.Name,
				Icon = this.Icon,
				DisplayColor = this.DisplayColor,
				TooltipParams = this.TooltipParams,
				RequiredLevel = this.RequiredLevel,
				TypeName = this.TypeName,
				Type = this.Type,
				Gems = this.Gems
			};
		}
	}
	#endregion

	#region ItemType
	/// <summary>
	/// {
	///		"id": "Mace",
	///		"twoHanded": false
	///	}
	/// </summary>
	[JsonObject]
	public class ItemType
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("twoHanded")]
		public string TwoHanded { get; set; }
	}
	#endregion

	#region FloatMinMax
	/// <summary>
	/// {
	///		"min": 941.4999839663506,
	///		"max": 941.4999839663506
	///	},
	/// </summary>
	[JsonObject]
	public class FloatMinMax
	{
		[JsonProperty("min")]
		public double Min { get; set; }

		[JsonProperty("max")]
		public double Max { get; set; }
	}
	#endregion

	#region IntegerMinMax
	/// <summary>
	/// {
	///		"min": 941.4999839663506,
	///		"max": 941.4999839663506
	///	},
	/// </summary>
	[JsonObject]
	public class IntegerMinMax
	{
		[JsonProperty("min")]
		public uint Min { get; set; }

		[JsonProperty("max")]
		public uint Max { get; set; }
	}
	#endregion

	#region SalvageItem
	/// <summary>
	/// {
	///		"id": "Crafting_Tier_04C",
	///		"name": "Iridescent Tear",
	///		"icon": "crafting_tier_04c",
	///		"displayColor": "yellow",
	///		"tooltipParams": "item/iridescent-tear"
	///	},
	/// </summary>
	[JsonObject]
	public class ItemSalvage
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("icon")]
		public string Icon { get; set; }

		[JsonProperty("displayColor")]
		public string DisplayColor { get; set; }

		[JsonProperty("tooltipParams")]
		public string TooltipParams { get; set; }
	}
	#endregion

	#region Gem
	/// <summary>
	/// {
	///		"item": {},
	///		"attributesRaw": {},
	///		"attributes": []
	///	}
	/// </summary>
	[JsonObject]
	public class Gem
	{
		[JsonProperty("item")]
		public ItemBrief Item { get; set; }

		[JsonProperty("attributesRaw")]
		public GemAttributesRaw AttributesRaw { get; set; }

		[JsonProperty("attributes")]
		public string[] Attributes { get; set; }

		#region GemAttributesRaw
		/// <summary>
		/// {
		///		"Hitpoints_Max_Percent_Bonus_Item": {}
		///	}
		/// </summary>
		[JsonObject]
		public class GemAttributesRaw
		{
		}
		#endregion
	}
	#endregion

	public enum ItemSlot
	{
		None,
		Head,
		Torso,
		Feet,
		Hands,
		Shoulders,
		Legs,
		Bracers,
		MainHand,
		OffHand,
		Waist,
		RightFinger,
		LeftFinger,
		Neck
	}
}
