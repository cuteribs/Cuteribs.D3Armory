﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CuteRibs.D3Armory.Models
{
	public class ProfileGroup : ObservableCollection<ProfileBrief>, INotifyPropertyChanged
	{
		private Realm realm;

		public Realm Realm
		{
			get { return this.realm; }
			set
			{
				if (this.realm != value)
				{
					this.realm = value;
					this.NotifyPropertyChanged("Realm");
				}
			}
		}

		public ObservableCollection<ProfileBrief> Profiles { get; set; }

		public ProfileGroup(Realm realm, IEnumerable<ProfileBrief> items)
		{
			this.Realm = realm;
			this.Profiles = new ObservableCollection<ProfileBrief>(items);
		}

		public override bool Equals(object obj)
		{
			var group = obj as ProfileGroup;
			return (group != null) && (this.Realm.Equals(group.Realm));
		}

		public override int GetHashCode()
		{
			return this.Realm.GetHashCode();
		}

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		protected void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion
	}
}
