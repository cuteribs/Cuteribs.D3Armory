﻿using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	[JsonObject]
	public class D3ObjectBase
	{
		[JsonProperty("code")]
		public string ErrorCode { get; set; }

		[JsonProperty("reason")]
		public string ErrorReason { get; set; }
	}
}
