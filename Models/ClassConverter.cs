﻿using System;
using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	public class ClassConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Class);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return new Class(reader.Value as string);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var obj = value as Class;

			if (obj == null)
			{
				throw new Exception("Expected Class object.");
			}

			writer.WriteValue(obj.Id);
		}
	}
}
