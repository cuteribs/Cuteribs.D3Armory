﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	#region Hero
	/// <summary>
	/// {
	///		"id": 6875500,
	///		"name": "Valkyrie",
	///		"class": "demon-hunter",
	///		"gender": 1,
	///		"level": 60,
	///		"hardcore": false,
	///		"skills": {},
	///		"items": {},
	///		"followers": {},
	///		"stats": {},
	///		"kills": {},
	///		"progress": {},
	///		"last-updated": 1342947987,
	///		"dead": false
	///	}
	/// </summary>
	[JsonObject]
	public class Hero : D3ObjectBase
	{
		[JsonProperty("id")]
		public uint Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("class")]
		[JsonConverter(typeof(ClassConverter))]
		public Class Class { get; set; }

		[JsonProperty("gender")]
		public byte Gender { get; set; }

		[JsonProperty("level")]
		public ushort Level { get; set; }

		[JsonProperty("hardcore")]
		public bool Hardcore { get; set; }

		[JsonProperty("skills")]
		public HeroSkills Skills { get; set; }

		[JsonProperty("items")]
		public HeroItems Items { get; set; }

		[JsonProperty("followers")]
		public HeroFollowers Followers { get; set; }

		[JsonProperty("stats")]
		public HeroStats Stats { get; set; }

		[JsonProperty("kills")]
		public HeroKills Kills { get; set; }

		[JsonProperty("progress")]
		public GameProgression Progress { get; set; }

		[JsonProperty("last-updated")]
		[JsonConverter(typeof(D3DateTimeConverter))]
		public DateTime LastUpdated { get; set; }

		[JsonProperty("dead")]
		public bool Dead { get; set; }

		#region HeroSkills
		/// <summary>
		/// {
		///		"active": [],
		///		"passive": []
		///	},
		/// </summary>
		[JsonObject]
		public class HeroSkills
		{
			[JsonProperty("active")]
			public SkillSet[] Active { get; set; }

			[JsonProperty("passive")]
			public SkillSet[] Passive { get; set; }
		}
		#endregion

		#region HeroItems
		/// <summary>
		/// {
		///		"head": {},
		///		"torso": {},
		///		"feet": {},
		///		"hands": {},
		///		"shoulders": {},
		///		"legs": {},
		///		"bracers": {},
		///		"mainHand": {},
		///		"offHand": {},
		///		"waist": {},
		///		"rightFinger": {},
		///		"leftFinger": {},
		///		"neck": {}
		///	}
		/// </summary>
		[JsonObject]
		public class HeroItems
		{
			[JsonProperty("head")]
			[ItemSlot(ItemSlot.Head)]
			public ItemBrief Head { get; set; }

			[JsonProperty("torso")]
			[ItemSlot(ItemSlot.Torso)]
			public ItemBrief Torso { get; set; }

			[JsonProperty("feet")]
			[ItemSlot(ItemSlot.Feet)]
			public ItemBrief Feet { get; set; }

			[JsonProperty("hands")]
			[ItemSlot(ItemSlot.Hands)]
			public ItemBrief Hands { get; set; }

			[JsonProperty("shoulders")]
			[ItemSlot(ItemSlot.Shoulders)]
			public ItemBrief Shoulders { get; set; }

			[JsonProperty("legs")]
			[ItemSlot(ItemSlot.Legs)]
			public ItemBrief Legs { get; set; }

			[JsonProperty("bracers")]
			[ItemSlot(ItemSlot.Bracers)]
			public ItemBrief Bracers { get; set; }

			[JsonProperty("mainHand")]
			[ItemSlot(ItemSlot.MainHand)]
			public ItemBrief MainHand { get; set; }

			[JsonProperty("offHand")]
			[ItemSlot(ItemSlot.OffHand)]
			public ItemBrief OffHand { get; set; }

			[JsonProperty("waist")]
			[ItemSlot(ItemSlot.Waist)]
			public ItemBrief Waist { get; set; }

			[JsonProperty("rightFinger")]
			[ItemSlot(ItemSlot.RightFinger)]
			public ItemBrief RightFinger { get; set; }

			[JsonProperty("leftFinger")]
			[ItemSlot(ItemSlot.LeftFinger)]
			public ItemBrief LeftFinger { get; set; }

			[JsonProperty("neck")]
			[ItemSlot(ItemSlot.Neck)]
			public ItemBrief Neck { get; set; }
		}
		#endregion

		#region HeroFollowers
		/// <summary>
		///	{
		///		"templar": {},
		///		"scoundrel": {},
		///		"enchantress": {}
		///	}
		/// </summary>
		[JsonObject]
		public class HeroFollowers
		{
			[JsonProperty("templar")]
			public Follower Templar { get; set; }

			[JsonProperty("scoundrel")]
			public Follower Scoundrel { get; set; }

			[JsonProperty("enchantress")]
			public Follower Enchantress { get; set; }
		}
		#endregion

		#region HeroStats
		/// <summary>
		/// {
		///		"life": 33335,
		///		"damage": 24694.6,
		///		"armor": 2505,
		///		"strength": 331,
		///		"dexterity": 1437,
		///		"vitality": 874,
		///		"intelligence": 426,
		///		"physicalResist": 71,
		///		"fireResist": 76,
		///		"coldResist": 114,
		///		"lightningResist": 37,
		///		"poisonResist": 114,
		///		"arcaneResist": 37,
		///		"damageIncrease": 14.369999885559082,
		///		"critChance": 0.07999999821186066,
		///		"damageReduction": 0.45504099130630493
		///	}
		/// </summary>
		[JsonObject]
		public class HeroStats
		{
			[JsonProperty("life")]
			public uint Life { get; set; }

			[JsonProperty("damage")]
			public uint Damage { get; set; }

			[JsonProperty("armor")]
			public uint Armor { get; set; }

			[JsonProperty("strength")]
			public uint Strength { get; set; }

			[JsonProperty("dexterity")]
			public uint Dexterity { get; set; }

			[JsonProperty("vitality")]
			public uint Vitality { get; set; }

			[JsonProperty("intelligence")]
			public uint Intelligence { get; set; }

			[JsonProperty("physicalResist")]
			public uint PhysicalResist { get; set; }

			[JsonProperty("fireResist")]
			public uint FireResist { get; set; }

			[JsonProperty("coldResist")]
			public uint ColdResist { get; set; }

			[JsonProperty("lightningResist")]
			public uint LightningResist { get; set; }

			[JsonProperty("poisonResist")]
			public uint PoisonResist { get; set; }

			[JsonProperty("arcaneResist")]
			public uint ArcaneResist { get; set; }

			[JsonProperty("damageIncrease")]
			public double DamageIncrease { get; set; }

			[JsonProperty("critChance")]
			public double CritChance { get; set; }

			[JsonProperty("damageReduction")]
			public double DamageReduction { get; set; }
		}
		#endregion

		#region HeroKills
		/// <summary>
		/// {
		///		"elites": 4019,
		///	}
		/// </summary>
		[JsonObject]
		public class HeroKills
		{
			[JsonProperty("elites")]
			public float Elites { get; set; }
		}
		#endregion
	} 
	#endregion

	#region Follower
	/// <summary>
	///	{
	///		"slug": "templar",
	///		"level": 60,
	///		"items": {},
	///		"stats": {},
	///		"skills": []
	///	},
	/// </summary>
	[JsonObject]
	public class Follower
	{
		[JsonProperty("slug")]
		public string Slug { get; set; }

		[JsonProperty("level")]
		public byte Level { get; set; }

		[JsonProperty("items")]
		public FollowerItems Items { get; set; }

		[JsonProperty("stats")]
		public FollowerStats Stats { get; set; }

		[JsonProperty("skills")]
		public FollowerSkill[] Skills { get; set; }

		#region FollowerItems
		/// <summary>
		/// {
		///		"special": {},
		///		"mainHand": {},
		///		"offHand": {},
		///		"rightFinger": {},
		///		"leftFinger": {},
		///		"neck": {}
		///	}
		/// </summary>
		[JsonObject]
		public class FollowerItems
		{
			[JsonProperty("special")]
			public ItemBrief Special { get; set; }

			[JsonProperty("mainHand")]
			public ItemBrief MainHand { get; set; }

			[JsonProperty("offHand")]
			public ItemBrief OffHand { get; set; }

			[JsonProperty("rightFinger")]
			public ItemBrief RightFinger { get; set; }

			[JsonProperty("leftFinger")]
			public ItemBrief LeftFinger { get; set; }

			[JsonProperty("neck")]
			public ItemBrief Neck { get; set; }
		}
		#endregion

		#region FollowerStats
		/// <summary>
		/// {
		///		"goldFind": 3,
		///		"magicFind": 0,
		///		"experienceBonus": 0
		///	}
		/// </summary>
		[JsonObject]
		public class FollowerStats
		{
			[JsonProperty("goldFind")]
			public uint GoldFind { get; set; }

			[JsonProperty("magicFind")]
			public uint MagicFind { get; set; }

			[JsonProperty("experienceBonus")]
			public uint ExperienceBonus { get; set; }
		}
		#endregion

		#region FollowerSkill
		/// <summary>
		/// {
		///		"slug": "heal",
		///		"name": "Heal",
		///		"icon": "templar_heal",
		///		"level": 5,
		///		"tooltipUrl": "skillSet/templar/heal",
		///		"description": "Heals you or the Templar for 4651 Life.\r\n\r\nCooldown: 30 seconds",
		///		"simpleDescription": "Heals you or the Templar.",
		///		"skillCalcId": "a"
		///	}
		/// </summary>
		[JsonObject]
		public class FollowerSkill
		{
			[JsonProperty("slug")]
			public string Slug { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }

			[JsonProperty("icon")]
			public string Icon { get; set; }

			[JsonProperty("level")]
			public byte Level { get; set; }

			[JsonProperty("tooltipUrl")]
			public string TooltipUrl { get; set; }

			[JsonProperty("description")]
			public string Description { get; set; }

			[JsonProperty("simpleDescription")]
			public string SimpleDescription { get; set; }

			[JsonProperty("skillCalcId")]
			public string SkillCalcId { get; set; }
		}
		#endregion
	}
	#endregion

	#region ItemBrief
	/// <summary>
	/// {
	///		"id": "Mace_1H_206",
	///		"name": "Virile Smite",
	///		"icon": "mace_1h_206",
	///		"displayColor": "yellow",
	///		"tooltipParams": "item/CMCl97kPEgcIBBVFFEudHbnx7BodnaLozB2HxBbvHbWWzuYiCwgAFc7-AQAYHiA-MAk46AVAAFAMYOgF",
	///		"requiredLevel": 60,
	///		"typeName": "Rare Mace",
	///		"type": 
	///		{
	///			"id": "Mace",
	///			"twoHanded": false
	///		}
	///	}
	/// </summary>
	[JsonObject]
	public class ItemBrief
	{
		public ItemSlot ItemSlot { get; set; }

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("icon")]
		public string Icon { get; set; }

		[JsonProperty("displayColor")]
		public string DisplayColor { get; set; }

		[JsonProperty("tooltipParams")]
		public string TooltipParams { get; set; }

		[JsonProperty("requiredLevel")]
		public byte RequiredLevel { get; set; }

		[JsonProperty("typeName")]
		public string TypeName { get; set; }

		[JsonProperty("type")]
		public ItemType Type { get; set; }

		[JsonProperty("gems")]
		public Gem[] Gems { get; set; }

		public ItemBrief()
		{
			var itemSlotAttibute = Attribute.GetCustomAttribute(this.GetType().Assembly, typeof(ItemSlotAttribute)) as ItemSlotAttribute;

			if (itemSlotAttibute != null)
			{
				this.ItemSlot = itemSlotAttibute.ItemSlot;
			}
		}
	}
	#endregion

	#region SkillSet
	/// <summary>
	/// {
	///		"skill": {}
	///		"rune": {}
	///	}
	/// </summary>
	[JsonObject]
	public class SkillSet
	{
		[JsonProperty("skill")]
		public ActiveSkill Skill { get; set; }

		[JsonProperty("rune")]
		public Rune Rune { get; set; }
	} 
	#endregion

	#region Skill
	/// <summary>
	/// {
	///		"slug": "hungering-arrow",
	///		"name": "Hungering Arrow",
	///		"icon": "demonhunter_hungeringarrow",
	///		"level": 1,
	///		"tooltipUrl": "skillSet/demon-hunter/hungering-arrow",
	///		"description": "Generate: 3 Hatred\r\n\r\nFire a magically imbued arrow that seeks out targets for 115% weapon damage and has		    ///a  35% chance to pierce through targets.",
	///		"skillCalcId": "a"
	///	}
	/// </summary>
	[JsonObject]
	public class Skill
	{
		[JsonProperty("slug")]
		public string Slug { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("icon")]
		public string Icon { get; set; }

		[JsonProperty("level")]
		public byte Level { get; set; }

		[JsonProperty("tooltipUrl")]
		public string TooltipUrl { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("skillCalcId")]
		public string SkillCalcId { get; set; }
	}
	#endregion

	#region ActiveSkill
	/// <summary>
	/// {
	///		"slug": "hungering-arrow",
	///		"name": "Hungering Arrow",
	///		"icon": "demonhunter_hungeringarrow",
	///		"level": 1,
	///		"categorySlug": "primary",
	///		"tooltipUrl": "skillSet/demon-hunter/hungering-arrow",
	///		"description": "Generate: 3 Hatred\r\n\r\nFire a magically imbued arrow that seeks out targets for 115% weapon damage and has		    ///a  35% chance to pierce through targets.",
	///		"simpleDescription": "Generate: 3 Hatred\r\n\r\nFire a piercing arrow that seeks additional targets.",
	///		"skillCalcId": "a"
	///	},
	/// </summary>
	[JsonObject]
	public class ActiveSkill : Skill
	{
		[JsonProperty("categorySlug")]
		public string CategorySlug { get; set; }

		[JsonProperty("simpleDescription")]
		public string SimpleDescription { get; set; }
	}
	#endregion

	#region PassiveSkill
	/// <summary>
	/// {
	///		"slug": "steady-aim",
	///		"name": "Steady Aim",
	///		"icon": "demonhunter_passive_steadyaim",
	///		"tooltipUrl": "skillSet/demon-hunter/steady-aim",
	///		"description": "As long as there are no enemies within 10 yards, all damage is increased by 20%.",
	///		"flavor": "\"Flee if you can, demon. I have a quiver full of friends who fly faster than you.\" —Tyla Shrikewing",
	///		"skillCalcId": "Y",
	///		"level": 16
	///	}
	/// </summary>
	[JsonObject]
	public class PassiveSkill : Skill
	{
		[JsonProperty("flavor")]
		public string Flavor { get; set; }
	}
	#endregion

	#region Rune
	/// <summary>
	/// {
	///		"slug": "spike-trap-d",
	///		"type": "d",
	///		"name": "Scatter",
	///		"level": 55,
	///		"description": "Simultaneously place all 3 traps.",
	///		"simpleDescription": "Simultaneously place multiple traps.",
	///		"tooltipParams": "rune/spike-trap/d",
	///		"skillCalcId": "c",
	///		"order": 4
	///	}
	/// </summary>
	[JsonObject]
	public class Rune
	{
		[JsonProperty("slug")]
		public string Slug { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("level")]
		public byte Level { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("simpleDescription")]
		public string SimpleDescription { get; set; }

		[JsonProperty("tooltipParams")]
		public string TooltipParams { get; set; }

		[JsonProperty("skillCalcId")]
		public string SkillCalcId { get; set; }

		[JsonProperty("order")]
		public byte Order { get; set; }
	}
	#endregion
}
