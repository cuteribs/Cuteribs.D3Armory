﻿using System;

namespace CuteRibs.D3Armory.Models
{
	public class Realm : IEquatable<Realm>, IComparable<Realm>
	{
		public static readonly Realm Americas = new Realm(Region.Americas);
		public static readonly Realm Europe = new Realm(Region.Europe);
		public static readonly Realm Asia = new Realm(Region.Asia);

		public Region Region { get; private set; }

		public string Name { get; set; }

		private Realm(Region region)
		{
			this.Region = region;
			this.Name = App.GetRealmName(this);
		}

		public override int GetHashCode()
		{
			return this.Region.GetHashCode();
		}

		public override string ToString()
		{
			return this.Region.ToString();
		}

		public bool Equals(Realm other)
		{
			return this.Region.Equals(other.Region);
		}

		public int CompareTo(Realm other)
		{
			return this.Region.CompareTo(other.Region);
		}

		public static Realm Parse(string value)
		{
			var region = (Region)Enum.Parse(typeof(Region), value, true);
			return new Realm(region);
		}
	}

	public enum Region
	{
		Americas,
		Europe,
		Asia
	}
}
