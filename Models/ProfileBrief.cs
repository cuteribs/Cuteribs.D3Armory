﻿using System;
using Newtonsoft.Json;

namespace CuteRibs.D3Armory.Models
{
	/// <summary>
	/// {
	///		"realm": "Americas",
	///		"battleTag": "Cuteribs#1554",
	///		"lastUpdated": 1342947987
	///	}
	/// </summary>
	[JsonObject]
	public class ProfileBrief
	{
		[JsonProperty("realm")]
		[JsonConverter(typeof(RealmConverter))]
		public Realm Realm { get; set; }

		[JsonProperty("battleTag")]
		[JsonConverter(typeof(BattleTagConverter))]
		public BattleTag BattleTag { get; set; }

		[JsonProperty("lastUpdated")]
		[JsonConverter(typeof(D3DateTimeConverter))]
		public DateTime LastUpdated { get; set; }
	}
}
