﻿using System;

namespace CuteRibs.D3Armory.Models
{
	[AttributeUsage(AttributeTargets.Property)]
	sealed class ItemSlotAttribute : Attribute
	{
		private readonly ItemSlot itemSlot;

		public ItemSlotAttribute(ItemSlot itemSlot)
		{
			this.itemSlot = itemSlot;
		}

		public ItemSlot ItemSlot
		{
			get { return this.itemSlot; }
		}
	}
}
