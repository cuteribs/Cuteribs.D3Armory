﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CuteRibs.D3Armory.Models
{
	#region Profile
	/// <summary>
	/// {
	///		"heroes": [],
	///		"lastHeroPlayed": 6875500,
	///		"lastUpdated": 1342947987,
	///		"artisans": [],
	///		"hardcoreArtisans": [],
	///		"kills": {},
	///		"timePlayed": {},
	///		"fallenHeroes": [],
	///		"battleTag": "Cuteribs#1554",
	///		"progression": {},
	///		"hardcoreProgression": {}
	///	}
	/// </summary>
	[JsonObject]
	public class Profile : D3ObjectBase
	{
		[JsonIgnore]
		public Realm Realm { get; set; }

		[JsonProperty("heroes")]
		public HeroBrief[] Heroes { get; set; }

		[JsonProperty("lastHeroPlayed")]
		public uint LastHeroPlayed { get; set; }

		[JsonProperty("lastUpdated")]
		[JsonConverter(typeof(D3DateTimeConverter))]
		public DateTime LastUpdated { get; set; }

		[JsonProperty("artisans")]
		public Artisan[] Artisans { get; set; }

		[JsonProperty("hardcoreArtisans")]
		public Artisan[] HardcoreArtisans { get; set; }

		[JsonProperty("kills")]
		public GameKills Kills { get; set; }

		[JsonProperty("timePlayed")]
		public GameTimePlayed TimePlayed { get; set; }

		[JsonProperty("fallenHeroes")]
		public HeroBrief[] FallenHeroes { get; set; }

		[JsonProperty("battleTag")]
		[JsonConverter(typeof(BattleTagConverter))]
		public BattleTag BattleTag { get; set; }

		[JsonProperty("progression")]
		public GameProgression Progression { get; set; }

		[JsonProperty("hardcoreProgression")]
		public GameProgression HardcoreProgression { get; set; }

		public ProfileBrief CreateBrief()
		{
			return new ProfileBrief
			{
				Realm = this.Realm,
				BattleTag = this.BattleTag,
				LastUpdated = this.LastUpdated
			};
		}
	} 
	#endregion

	#region HeroBrief
	/// <summary>
	/// {
	///		"name": "Valkyrie",
	///		"id": 6875500,
	///		"level": 60,
	///		"hardcore": false,
	///		"class": "demon-hunter",
	///		"gender": 1,
	///		"last-updated": 1342947987,
	///		"dead": false
	///	}
	/// </summary>
	[JsonObject]
	public class HeroBrief
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("id")]
		public uint Id { get; set; }

		[JsonProperty("level")]
		public ushort Level { get; set; }

		[JsonProperty("hardcore")]
		public bool Hardcore { get; set; }

		[JsonProperty("class")]
		[JsonConverter(typeof(ClassConverter))]
		public Class Class { get; set; }

		[JsonProperty("gender")]
		public byte Gender { get; set; }

		[JsonProperty("last-updated")]
		[JsonConverter(typeof(D3DateTimeConverter))]
		public DateTime LastUpdated { get; set; }

		[JsonProperty("dead")]
		public bool Dead { get; set; }
	} 
	#endregion

	#region GameProgression
	/// <summary>
	/// {
	///		"normal": {},
	///		"nightmare": {},
	///		"hell": {},
	///		"inferno": {}
	///	}
	/// </summary>
	[JsonObject]
	public class GameProgression
	{
		[JsonProperty("normal")]
		public LevelProgression Normal { get; set; }

		[JsonProperty("nightmare")]
		public LevelProgression Nightmare { get; set; }

		[JsonProperty("hell")]
		public LevelProgression Hell { get; set; }

		[JsonProperty("inferno")]
		public LevelProgression Inferno { get; set; }
	}

	/// <summary>
	/// {
	///		"act1": {},
	///		"act2": {},
	///		"act3": {},
	///		"act4": {}
	///	}
	/// </summary>
	[JsonObject]
	public class LevelProgression
	{
		[JsonProperty("act1")]
		public ActProgression Act1 { get; set; }

		[JsonProperty("act2")]
		public ActProgression Act2 { get; set; }

		[JsonProperty("act3")]
		public ActProgression Act3 { get; set; }

		[JsonProperty("act4")]
		public ActProgression Act4 { get; set; }
	}

	/// <summary>
	/// {
	///		"completed": true,
	///		"completedQuests": []
	///	}
	/// </summary>
	[JsonObject]
	public class ActProgression
	{
		[JsonProperty("completed")]
		public bool Completed { get; set; }

		[JsonProperty("completedQuests")]
		public Quest[] CompletedQuests { get; set; }
	}

	/// <summary>
	/// {
	///		"slug": "the-fallen-star",
	///		"name": "The Fallen Star"
	///	},
	/// </summary>
	[JsonObject]
	public class Quest
	{
		[JsonProperty("slug")]
		public string Slug { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }
	} 
	#endregion

	#region GameTimePlayed
	/// <summary>
	/// {
	///		"barbarian": 0.002,
	///		"demon-hunter": 1,
	///		"monk": 0.002,
	///		"witch-doctor": 0.188,
	///		"wizard": 0.808
	///	}
	/// </summary>
	[JsonObject]
	public class GameTimePlayed
	{
		[JsonProperty("barbarian")]
		public float Barbarian { get; set; }

		[JsonProperty("demon-hunter")]
		public float DemonHunter { get; set; }

		[JsonProperty("monk")]
		public float Monk { get; set; }

		[JsonProperty("witch-doctor")]
		public float WitchDoctor { get; set; }

		[JsonProperty("wizard")]
		public float Wizard { get; set; }
	}
	#endregion

	#region GameKills
	/// <summary>
	/// {
	///		"monsters": 106211,
	///		"elites": 5266,
	///		"hardcoreMonsters": 0
	///	}
	/// </summary>
	[JsonObject]
	public class GameKills
	{
		[JsonProperty("monsters")]
		public ulong Monsters { get; set; }

		[JsonProperty("elites")]
		public ulong Elites { get; set; }

		[JsonProperty("hardcoreMonsters")]
		public ulong HardcoreMonsters { get; set; }
	}
	#endregion

	#region Artisan
	/// <summary>
	/// {
	///		"slug": "blacksmith",
	///		"level": 10,
	///		"stepCurrent": 0,
	///		"stepMax": 5
	///	}
	/// </summary>
	[JsonObject]
	public class Artisan
	{
		[JsonProperty("slug")]
		public string Slug { get; set; }

		[JsonProperty("level")]
		public byte Level { get; set; }

		[JsonProperty("stepCurrent")]
		public short StepCurrent { get; set; }

		[JsonProperty("stepMax")]
		public short StepMax { get; set; }
	}
	#endregion

}
